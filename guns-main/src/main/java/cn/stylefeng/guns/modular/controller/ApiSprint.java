package cn.stylefeng.guns.modular.controller;

import cn.stylefeng.guns.sys.core.enums.SwaggerGroupInterface;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * @author wsp
 */
@Component
public class ApiSprint implements SwaggerGroupInterface {

    /**
     * swagger 文档分组
     *
     * @return
     */
    @Override
    public Object[] getSwaggerGroupList() {
          return Arrays.stream(ApiSprint.class.getDeclaredFields()).map(ApiSprint::getValue).toArray();
    }

    private static Object getValue(Field field) {
        try {
            return field.get(ApiSprint.class);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }
}
