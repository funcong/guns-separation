package cn.stylefeng.guns.core.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

public interface BaseEnum extends IEnum<Integer> {
    /**
     * 枚举实例名，例如：man、woman
     * @return
     */
    String getCode();

    /**
     * 枚举中文释义，例如：男、女
     * @return
     */
    String getName();
}
