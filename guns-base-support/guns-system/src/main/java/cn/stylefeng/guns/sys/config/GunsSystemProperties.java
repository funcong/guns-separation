package cn.stylefeng.guns.sys.config;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * guns项目配置
 *
 * @author stylefeng
 * @Date 2017/5/23 22:31
 */
@Data
@Component
@ConfigurationProperties(prefix = GunsSystemProperties.PREFIX)
public class GunsSystemProperties {

    public static final String PREFIX = "yxl";

    private String ossBucket;

    private String ossUrl;

    private String ossDir;

    private Boolean swaggerOpen = false;

    private String fileUploadPath;

    private String siteUrl;

    /**
     * 生产环境默认true，加快首次接口调用速度；<br/>
     * 其他环境默认false，加快项目启动速度。
     */
    private Boolean initDingTalkOnStart;

    private Boolean haveCreatePath = false;

    public String getFileUploadPath() {
        //如果没有写文件上传路径,保存到临时目录
        if (StrUtil.isBlank(fileUploadPath)) {
            return System.getProperty("java.io.tmpdir");
        } else {
            //判断有没有结尾符,没有得加上
            if (!fileUploadPath.endsWith(File.separator)) {
                fileUploadPath = fileUploadPath + File.separator;
            }
            //判断目录存不存在,不存在得加上
            if (!haveCreatePath) {
                File file = new File(fileUploadPath);
                file.mkdirs();
                haveCreatePath = true;
            }
            return fileUploadPath;
        }
    }
}
