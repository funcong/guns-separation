package cn.stylefeng.guns.sys.core.enums;

/**
 * swagger文档分组接口，各服务中请实现该接口并注册为@Component
 * @author wsp
 */
public interface SwaggerGroupInterface {
    /**
     * swagger 文档分组
     *
     * @return
     */
    Object[] getSwaggerGroupList();
}
